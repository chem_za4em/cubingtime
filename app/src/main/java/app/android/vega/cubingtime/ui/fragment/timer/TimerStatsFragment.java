package app.android.vega.cubingtime.ui.fragment.timer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import app.android.vega.cubingtime.R;
import app.android.vega.cubingtime.presentation.view.timer.TimerStatsView;
import app.android.vega.cubingtime.presentation.presenter.timer.TimerStatsPresenter;

import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

public class TimerStatsFragment extends MvpFragment implements TimerStatsView {
    public static final String TAG = "TimerStatsFragment";
    @InjectPresenter
    TimerStatsPresenter mTimerStatsPresenter;

    public static TimerStatsFragment newInstance() {
        TimerStatsFragment fragment = new TimerStatsFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_timer_stats, container, false);
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
