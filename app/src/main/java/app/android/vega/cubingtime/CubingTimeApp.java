package app.android.vega.cubingtime;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * Created by chem on 03.08.17.
 */

public class CubingTimeApp extends Application {

    public static SharedPreferences settings;

    private static Cicerone<Router> cicerone;

    public static String currentScreen = "";


    @Override
    public void onCreate() {
        super.onCreate();
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        initCicerone();
    }

    private void initCicerone() {
        cicerone = Cicerone.create();
    }

    public static NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public static Router getRouter() {
        return cicerone.getRouter();
    }
}
