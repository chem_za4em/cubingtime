package app.android.vega.cubingtime.ui.fragment.timer;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import app.android.vega.cubingtime.R;
import app.android.vega.cubingtime.presentation.presenter.timer.TimerContainerPresenter;
import app.android.vega.cubingtime.presentation.view.timer.TimerContainerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TimerContainerFragment extends MvpFragment implements TimerContainerView {
    public static final String TAG = "TimerContainerFragment";
    @InjectPresenter
    TimerContainerPresenter presenter;
    @BindView(R.id.timer_tab_layout) TabLayout tabLayout;
    @BindView(R.id.timer_view_pager) ViewPager viewPager;
    Unbinder unbinder;
    SparseArray<Fragment> timerFragments;
    SparseArray<String> fragmentsTitle;


    public static TimerContainerFragment newInstance() {
        TimerContainerFragment fragment = new TimerContainerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timer_container, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        initViews();
        return view;
    }

    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.timer, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initViews() {
        initFragments();
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(getAdapter());
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initFragments() {
        timerFragments = new SparseArray<>();
        fragmentsTitle = new SparseArray<>();

        timerFragments.put(0, TimerFragment.newInstance());
        timerFragments.put(1, TimerStatsFragment.newInstance());

        fragmentsTitle.put(0, getString(R.string.timer));
        fragmentsTitle.put(1, getString(R.string.stats));
    }


    public FragmentPagerAdapter getAdapter() {
        return new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override public Fragment getItem(int position) {
                return timerFragments.get(position);
            }

            @Override public int getCount() {
                return timerFragments.size();
            }

            @Override public CharSequence getPageTitle(int position) {
                return fragmentsTitle.get(position);
            }
        };
    }


    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
