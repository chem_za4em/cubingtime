package app.android.vega.cubingtime.presentation.presenter.main;


import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import app.android.vega.cubingtime.Screens;
import app.android.vega.cubingtime.presentation.view.main.MainView;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    Router router;

    public MainPresenter(Router router) {
        this.router = router;
        router.newRootScreen(Screens.TIMER_CONTAINER);
    }

    public void onTimerNavigationClick() {
        router.replaceScreen(Screens.TIMER_CONTAINER);
    }

    public void onContestsNavigationClick() {
//        router.replaceScreen(Screens.CONTESTS_CONTAINER);
    }

    public void onBackPressed() {
        router.exit();
    }

}
