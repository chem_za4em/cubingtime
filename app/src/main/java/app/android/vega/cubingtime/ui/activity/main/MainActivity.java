package app.android.vega.cubingtime.ui.activity.main;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.HashMap;
import java.util.Map;

import app.android.vega.cubingtime.CubingTimeApp;
import app.android.vega.cubingtime.R;
import app.android.vega.cubingtime.Screens;
import app.android.vega.cubingtime.presentation.presenter.main.MainPresenter;
import app.android.vega.cubingtime.presentation.view.main.MainView;
import app.android.vega.cubingtime.ui.fragment.timer.TimerContainerFragment;
import app.android.vega.cubingtime.ui.fragment.timer.TimerStatsFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.FragmentNavigator;

public class MainActivity extends MvpAppCompatActivity
        implements MainView, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    private Map<String, MvpFragment> fragments;
    private TextView navigationHeaderFirstName;

    @InjectPresenter MainPresenter presenter;

    @ProvidePresenter
    public MainPresenter createMainPresenter() {
        return new MainPresenter(CubingTimeApp.getRouter());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initViews();
    }

    private void initViews() {
        fragments = new HashMap<>();
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationHeaderFirstName = navigationView.getHeaderView(0)
                .findViewById(R.id.nav_header_first_name);
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            presenter.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_timer:
                presenter.onTimerNavigationClick();
                break;
            case R.id.nav_contests:
                presenter.onContestsNavigationClick();
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override protected void onResume() {
        super.onResume();
        CubingTimeApp.getNavigatorHolder().setNavigator(navigator);
    }

    @Override protected void onPause() {
        super.onPause();
        CubingTimeApp.getNavigatorHolder().removeNavigator();
    }

    private Navigator navigator = new FragmentNavigator(getFragmentManager(),
            R.id.main_navigation_container) {
        @Override protected Fragment createFragment(String screenKey, Object data) {

            switch (screenKey) {
                case Screens.TIMER_CONTAINER:
                    if (!fragments.containsKey(screenKey))
                        fragments.put(screenKey, TimerContainerFragment.newInstance());
                    toolbar.setTitle(getString(R.string.timer));
                    break;
                case Screens.CONTESTS_CONTAINER:

                    break;
            }
            CubingTimeApp.currentScreen = screenKey;
            return fragments.get(screenKey);


        }

        @Override protected void showSystemMessage(String message) {

        }

        @Override protected void exit() {
            finish();
        }
    };
}
