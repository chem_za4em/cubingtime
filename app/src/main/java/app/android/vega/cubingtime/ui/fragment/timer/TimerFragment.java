package app.android.vega.cubingtime.ui.fragment.timer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.android.vega.cubingtime.R;
import app.android.vega.cubingtime.presentation.view.timer.TimerView;
import app.android.vega.cubingtime.presentation.presenter.timer.TimerPresenter;

import com.arellomobile.mvp.MvpFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

public class TimerFragment extends MvpFragment implements TimerView {
    public static final String TAG = "TimerFragment";
    @InjectPresenter
    TimerPresenter mTimerPresenter;

    public static TimerFragment newInstance() {
        TimerFragment fragment = new TimerFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_timer, container, false);
    }


    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
