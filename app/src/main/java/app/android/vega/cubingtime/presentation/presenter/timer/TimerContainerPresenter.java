package app.android.vega.cubingtime.presentation.presenter.timer;


import app.android.vega.cubingtime.presentation.view.timer.TimerContainerView;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class TimerContainerPresenter extends MvpPresenter<TimerContainerView> {

}
